<?php
//styles
function triana_styles(){

    $stylesrc = get_template_directory_uri() . '/assets/css/';
    $stylepath = get_template_directory() . '/assets/css/';
    wp_enqueue_style( 'bootstrap_css',  get_template_directory_uri(  ) . '/assets/css/bootstrap.min.css', array(), '5.1.1' );
    wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css2?family=Roboto:wght@300;400&display=swap', array(), '1.0.0' );
    wp_enqueue_style( 'owl_carousel',  get_template_directory_uri(  ) . '/assets/css/owl.carousel.min.css', array(), '2.3.4' );
    wp_enqueue_style( 'owl_theme',  get_template_directory_uri(  ) . '/assets/css/owl.theme.default.min.css', array(), '2.3.4' ); 
    wp_enqueue_style( 'ligthbox', 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css', array(), '2.10.0' );
    wp_enqueue_style( 'main',  $stylesrc . 'main.css', array(), filemtime($stylepath . 'main.css') );

    wp_enqueue_style( 'style', get_stylesheet_uri(), array('normalize', 'bootstrap_css', 'fonts', 'owl_css', 'animate', 'main'), '1.0.0' );

}
add_action('wp_enqueue_scripts', 'triana_styles');

//scripts
function triana_scripts(){

    $scriptsrc = get_template_directory_uri() . '/assets/js/';
    $scriptpath = get_template_directory() . '/assets/js/';
    wp_deregister_script('jquery');
    wp_register_script('jquery', includes_url('/js/jquery/jquery.js'), false, NULL, true);
    wp_enqueue_script('jquery');
    wp_register_script( 'bootstrap_js', $scriptsrc . 'bootstrap.bundle.min.js', false, '5.1.1', true); 
    wp_enqueue_script( 'bootstrap_js');
    wp_register_script( 'owl_js', $scriptsrc . 'owl.carousel.min.js', array( 'jquery' ), '2.3.4', true);  
    wp_enqueue_script('owl_js' );
    wp_register_script( 'rutjquery', $scriptsrc . 'rut.jquery.min.js', array( 'jquery' ), filemtime($scriptpath . 'rut.jquery.min.js'), true); 
    wp_enqueue_script( 'rutjquery' );
    wp_register_script( 'main', $scriptsrc . 'main.js', array( 'jquery' ), filemtime($scriptpath . 'main.js'), true); 
    wp_enqueue_script( 'main' );

}
add_action('wp_enqueue_scripts', 'triana_scripts');

function my_wpcf7_form_elements($html) {
    $text = 'Por favor seleccione una opción...';
    $html = str_replace('<option value="">---</option>', '<option value="">' . $text . '</option>', $html);
    return $html;
}
add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');