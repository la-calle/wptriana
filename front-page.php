<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
    <!-- section bannerHome -->
    <section id="bannerHome">
        <div class="container-fluid text-center">
            <div class="row g-0 mt-2 mb-2 mx-auto d-none d-sm-flex">
                <div class="col-12 d-lg-none d-xl-block ">
                    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner/deskbanner-min.png" alt="Triana Propiedades">
                </div>
            </div>

            <!-- Vista mobile -->
            <div class="row g-0 mt-2 mb-2 mx-auto d-xxl-none">
                <div class="col-12 d-sm-none d-md-none d-sm-block">
                    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/banner/mobbanner-min.png" alt="Triana Propiedades">
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h3 class="text-center mt-3">Filtra para buscar tu departamento:</h3>
                        <div class="row pt-2 pb-3 text-center">
                            <div class="col-6 col-lg-3 mb-2">
                                <a href="#rivne" class="nav-link nav-filter px-2 me-3 text-white w-100">Quinta Normal</a>
                            </div>
                            <div class="col-6 col-lg-3 mb-2">
                                <a href="#odesa" class="nav-link nav-filter px-2 me-3 text-white w-100">La Cisterna</a>
                            </div>
                            <div class="col-6 col-lg-3 mb-2">
                                <a href="#toesca" class="nav-link nav-filter px-2 me-3 text-white w-100">Santiago Centro</a>
                            </div>
                            <div class="col-6 col-lg-3 mb-2">
                                <a href="#zanartu" class="nav-link nav-filter px-2 me-3 text-white w-100">Ñuñoa</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section card -->
    <section id="cardTriana" class="mb-4">

        <div class="container-fluid" id="rivne">
            <div class="container py-3">
                <!-- Edificio uno -->
                <div class="row my-2">
                    <div class="col-12">
                        <div class="card mb-3 shadow-sm">
                            <div class="row g-0">
                                <!-- fotos departamento -->
                                <div class="col-md-6 pt-4 px-3 order-lg-first">
                                    <div class="owl-carousel owl-theme galeria-rivne">
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/quinta-normal/quinta-normal1078x800.png" data-lightbox="gallery-1">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/quinta-normal/quinta-normal539x400.png" alt="Edificio Rivne y Elena">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/1078x800/1.png" data-lightbox="gallery-1">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/539x400/1.png" alt="Edificio Rivne y Elena">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/1078x800/2.png" data-lightbox="gallery-1">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/539x400/2.png" alt="Edificio Rivne y Elena">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/1078x800/3.png" data-lightbox="gallery-1">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/539x400/3.png" alt="Edificio Rivne y Elena">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/1078x800/4.png" data-lightbox="gallery-1">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/539x400/4.png" alt="Edificio Rivne y Elena">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/1078x800/5.png" data-lightbox="gallery-1">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/539x400/5.png" alt="Edificio Rivne y Elena">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/1078x800/6.png" data-lightbox="gallery-1">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/539x400/6.png" alt="Edificio Rivne y Elena">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/1078x800/7.png" data-lightbox="gallery-1">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/quinta-normal/539x400/7.png" alt="Edificio Rivne y Elena">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- descripcion departamento -->
                                <div class="col-md-6 order-lg-last">
                                    <div class="card-body">
                                        <h2 class="card-title">Edificio Rivne y Elena</h2>
                                        <address class="address-edificio"><i class="fas fa-map-marker-alt"></i>
                                            Poeta
                                            Pedro Prado 1353, Quinta Normal</address>
                                        <div class="tabla rounded">
                                            <table class="table table-bordered table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th scope="col"><i class="fas fa-bed"></i> Dormitorios</th>
                                                        <th scope="col"><i class="fas fa-bath"></i> Baños</th>
                                                        <th scope="col"><i class="fas fa-dollar-sign"></i> Precio
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>desde <b>$300.000</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>1</td>
                                                        <td>desde <b>$320.000</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>1</td>
                                                        <td>desde <b>$350.000</b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="row g-1">
                                            <div class="col-12">
                                                <h5>Conectividad</h5>
                                                <p>Locomoción a dos cuadras, cercanía a estaciones de metro <b>Gruta
                                                        de
                                                        Lourdes</b> y <b>Quinta Normal de línea 5</b>.</p>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Quincho"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-quincho.png" alt="Quincho"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Salón de eventos"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-salon.png" alt="Salón de eventos"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Bicicleteros"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-bicicletero.png" alt="Bicicleteros"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Lavanderia"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-lavanderia.png" alt="Lavanderia"></button>

                                            </div>
                                            <div class="btn-group mb-3" role="group">
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Acceso control biométrico"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-biometrico.png" alt="Biometrico"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Seguridad 24x7"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-seguridad.png" alt="Seguridad 24x7"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Juegos infantiles"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-juegos.png" alt="Juegos infantiles"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Metro"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-metro.png" alt="Metro"></button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="btn-group w-100" role="group">
                                                    <a href="#contacto" class="btn btn-lg btn-secondary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3">
                                                        Quiero más información</a>
                                                    <a href="https://my.matterport.com/show/?m=Fj8Y2XaEiBX" target="_blank" rel="noopener nofollow" class="btn btn-lg btn-primary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3">Tour Virtual 360°</a>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="card-text"><small class="text-muted">*Revisa los requisitos para
                                                rentar un departamento</small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid bg-triana-light" id="odesa">
            <div class="container py-3">
                <!-- Edificio dos -->
                <div class="row my-5">
                    <div class="col-12">
                        <div class="card mb-3 shadow-sm">
                            <div class="row g-0">
                                <!-- fotos departamento -->
                                <div class="col-md-6 pt-4 px-3 order-lg-last">
                                    <div class="owl-carousel owl-theme galeria-odesa">
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/odesa/odesa-1078x800.png" data-lightbox="gallery-2">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/odesa/odesa-539x400.png" alt="Edificio Odesa">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/1078x800/3.png" data-lightbox="gallery-2">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/539x400/3.png" alt="Edificio Odesa">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/1078x800/2.png" data-lightbox="gallery-2">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/539x400/2.png" alt="Edificio Odesa">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/1078x800/1.png" data-lightbox="gallery-2">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/539x400/1.png" alt="Edificio Odesa">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/1078x800/4.png" data-lightbox="gallery-2">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/539x400/4.png" alt="Edificio Odesa">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/1078x800/5.png" data-lightbox="gallery-2">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/539x400/5.png" alt="Edificio Odesa">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/1078x800/7.png" data-lightbox="gallery-2">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/odesa/539x400/7.png" alt="Edificio Odesa">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- descripcion departamento -->
                                <div class="col-md-6 order-lg-first">
                                    <div class="card-body">
                                        <h2 class="card-title">Edificio Odesa</h2>
                                        <address class="address-edificio"><i class="fas fa-map-marker-alt"></i>
                                            Goycolea 1136, La Cisterna</address>
                                        <div class="tabla rounded">
                                            <table class="table table-bordered table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th scope="col"><i class="fas fa-bed"></i> Dormitorios</th>
                                                        <th scope="col"><i class="fas fa-bath"></i> Baños</th>
                                                        <th scope="col"><i class="fas fa-dollar-sign"></i> Precio
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>1</td>
                                                        <td>desde <b>$330.000</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>1</td>
                                                        <td>desde <b>$360.000</b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="row g-1">
                                            <div class="col-12">
                                                <h5>Conectividad</h5>
                                                <p>Locomoción a la puerta, cercanía a Estación Intermodal <b>Metro
                                                        La Cisterna</b>.</p>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Quincho"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-quincho.png" alt="Quincho"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Salón de eventos"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-salon.png" alt="Salón de eventos"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Bicicleteros"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-bicicletero.png" alt="Bicicleteros"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Lavanderia"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-lavanderia.png" alt="Lavanderia"></button>

                                            </div>
                                            <div class="btn-group mb-3" role="group">
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Acceso control biométrico"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-biometrico.png" alt="Biometrico"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Seguridad 24x6"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-seguridad.png" alt="Seguridad 24x6"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Juegos infantiles"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-juegos.png" alt="Juegos infantiles"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Metro"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-metro.png" alt="Metro"></button>
                                            </div>
                                        </div>
                                        <div class="btn-group w-100" role="group">
                                            <a href="#contacto" class="btn btn-lg btn-secondary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3">
                                                Quiero más información</a>
                                            <a href="#" class="btn btn-lg btn-primary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3">Próximamente Tour Virtual</a>
                                        </div>
                                        <p class="card-text"><small class="text-muted">*Revisa los requisitos para
                                                rentar un departamento</small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid" id="toesca">
            <div class="container py-3">
                <!-- Edificio tres -->
                <div class="row my-5">
                    <div class="col-12">
                        <div class="card mb-3 shadow-sm">
                            <div class="row g-0">
                                <!-- fotos departamento -->
                                <div class="col-md-6 pt-4 px-3 order-lg-first">
                                    <div class="owl-carousel owl-theme galeria-toesca">
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/toesca/toesca-1078x800.png" data-lightbox="gallery-3">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/toesca/toesca-539x400.png" alt="Edificio Toesca">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/1078x800/1.png" data-lightbox="gallery-3">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/539x400/1.png" alt="Edificio Toesca">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/1078x800/2.png" data-lightbox="gallery-3">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/539x400/2.png" alt="Edificio Toesca">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/1078x800/3.png" data-lightbox="gallery-3">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/539x400/3.png" alt="Edificio Toesca">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/1078x800/4.png" data-lightbox="gallery-3">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/539x400/4.png" alt="Edificio Toesca">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/1078x800/5.png" data-lightbox="gallery-3">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/539x400/5.png" alt="Edificio Toesca">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/1078x800/6.png" data-lightbox="gallery-3">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/539x400/6.png" alt="Edificio Toesca">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/1078x800/7.png" data-lightbox="gallery-3">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/toesca/539x400/7.png" alt="Edificio Toesca">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- descripcion departamento -->
                                <div class="col-md-6 order-lg-last">
                                    <div class="card-body">
                                        <h2 class="card-title">Edificio Toesca</h2>
                                        <address class="address-edificio"><i class="fas fa-map-marker-alt"></i>
                                            Toesca 2802, Santiago</address>
                                        <div class="tabla rounded">
                                            <table class="table table-bordered table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th scope="col"><i class="fas fa-bed"></i> Dormitorios</th>
                                                        <th scope="col"><i class="fas fa-bath"></i> Baños</th>
                                                        <th scope="col"><i class="fas fa-dollar-sign"></i> Precio
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>desde <b>$250.000</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>2</td>
                                                        <td>desde <b>$314.000</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>2</td>
                                                        <td>desde <b>$350.000</b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="row g-1">
                                            <div class="col-12">
                                                <h5>Conectividad</h5>
                                                <p>Locomoción a dos cuadras, cercanía a estaciones de metro <b> República</b> y <b>ULA (Unión Latino Americana)</b>.</p>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Piscina"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-piscina.png" alt="Piscina"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Gimnasio"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-gym.png" alt="Gimnasio"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Lavanderia"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-lavanderia.png" alt="Lavanderia"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Quincho"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-quincho.png" alt="Quincho"></button>


                                            </div>
                                            <div class="btn-group mb-3" role="group">
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Acceso control biométrico"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-biometrico.png" alt="Biometrico"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Seguridad 24x7"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-seguridad.png" alt="Seguridad 24x7"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Caja vecina & almacen interior"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-comercio.png" alt="almacen interior"></button>
                                            </div>
                                        </div>
                                        <div class="btn-group w-100" role="group">
                                            <a href="#contacto" class="btn btn-lg btn-secondary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3">
                                                Quiero más información</a>
                                            <a href="https://my.matterport.com/show/?m=JxVvuUQP6fZ" target="_blank" rel="noopener nofollow" class="btn btn-lg btn-primary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3">Tour Virtual 360°</a>
                                        </div>
                                        <p class="card-text"><small class="text-muted">*Revisa los requisitos para
                                                rentar un departamento</small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid bg-light" id="contacto-uno">
            <div class="container">
                <div class="row py-5">
                    <div class="col-lg-12 col-12">
                        <div class="card pb-3 bg-triana">
                            <div class="card-body px-5 text-white">
                                <div class="row">
                                    <h4 class="mb-4"><u>Requisitos para arrendar en Rivne y Elena, Odesa y Toesca</u></h4>
                                    <div class="col-lg-6 col-12">
                                        <h6>Promedio de Renta (se puede complementar entre los habitantes)</h6>
                                            <ul>
                                                <li>1 Ambiente: $600.000</li>
                                                <li>1 Dormitorio: $600.000</li>
                                                <li>2 Dormitorios: $800.000</li>
                                                <li>3 Dormitorios: $1.000.000</li>
                                            </ul>
                                    </div>
                                    <div class="col-lg-6 col-12">
                                        <h6>Antecedentes comerciales</h5>
                                            <ul>
                                                <li>Cédula de identidad o pasaporte</li>
                                                <li>Contrato de trabajo indefinido</li>
                                                <li>3 últimas liquidaciones de sueldo</li>
                                                <li>3 últimas cotizaciones AFP</li>
                                                <li>Carpeta tributaria (independientes)</li>
                                                <li>No tener antecedentes comerciales (DICOM)</li>
                                            </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid bg-triana-light" id="sazie">
            <div class="container py-3">
                <!-- Edificio cuatro -->
                <div class="row my-5">
                    <div class="col-12">
                        <div class="card mb-3 shadow-sm">
                            <div class="row g-0">
                                <!-- fotos departamento -->
                                <div class="col-md-6 pt-4 px-3 order-lg-last">
                                    <div class="owl-carousel owl-theme galeria-sazie">
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/sazie/sazie-1078x800.png" data-lightbox="gallery-4">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/sazie/sazie-539x400.png" alt="Edificio Sazie">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/1.png" data-lightbox="gallery-4">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/1.png" alt="Edificio Sazie">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/2.png" data-lightbox="gallery-4">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/2.png" alt="Edificio Sazie">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/3.png" data-lightbox="gallery-4">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/3.png" alt="Edificio Sazie">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/4.png" data-lightbox="gallery-4">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/4.png" alt="Edificio Sazie">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/5.png" data-lightbox="gallery-4">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/5.png" alt="Edificio Sazie">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/6.png" data-lightbox="gallery-4">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/6.png" alt="Edificio Sazie">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/7.png" data-lightbox="gallery-4">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/7.png" alt="Edificio Sazie">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- descripcion departamento -->
                                <div class="col-md-6 order-lg-first">
                                    <div class="card-body">
                                        <h2 class="card-title">Edificio Sazie</h2>
                                        <address class="address-edificio"><i class="fas fa-map-marker-alt"></i>
                                            Sazié 2357, Santiago</address>
                                        <div class="tabla rounded">
                                            <table class="table table-bordered table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th scope="col"><i class="fas fa-bed"></i> Ambientes</th>
                                                        <th scope="col"><i class="fas fa-dollar-sign"></i> Precio
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>desde <b>$290.000</b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="row g-1">
                                            <div class="col-12">
                                                <h5>Conectividad</h5>
                                                <p>Locomoción a la puerta, ubicado en pleno barrio universitario.
                                                    Cercanía a <b>Metro República</b>.</p>
                                            </div>
                                            <div class="btn-group" role="group">
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Quincho"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-quincho.png" alt="Quincho"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Lavanderia"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-lavanderia.png" alt="Lavanderia"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Bicicleteros"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-bicicletero.png" alt="Bicicleteros"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Cowork"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-coworking.png" alt="Cowork"></button>
                                            </div>
                                            <div class="btn-group mb-3" role="group">
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Acceso control biométrico"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-biometrico.png" alt="Biometrico"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Seguridad 24x7"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-seguridad.png" alt="Seguridad 24x7"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Metro"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-metro.png" alt="Metro"></button>
                                            </div>
                                        </div>
                                        <div class="btn-group w-100" role="group">
                                            <a href="sazie" class="btn btn-lg btn-secondary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3">
                                                Conocer más</a>
                                            <a href="https://my.matterport.com/show/?m=wU8LgMYpPbT" target="_blank" rel="noopener nofollow" class="btn btn-lg btn-primary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3">Tour Virtual 360°</a>
                                        </div>
                                        <p class="card-text"><small class="text-muted">*Revisa los requisitos para
                                                rentar un departamento</small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid" id="zanartu">
            <div class="container py-3">
                <!-- Edificio cinco -->
                <div class="row my-5">
                    <div class="col-12">
                        <div class="card mb-3 shadow-sm">
                            <div class="row g-0">
                                <!-- fotos departamento -->
                                <div class="col-md-6 pt-4 px-3 order-lg-first">
                                    <div class="owl-carousel owl-theme galeria-zanartu">
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/zanartu/zanartu-1078x800.png" data-lightbox="gallery-5">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/zanartu/zanartu-539x400.png" alt="Edificio Zañartu">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/1078x800/1.png" data-lightbox="gallery-5">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/539x400/1.png" alt="Edicio Zañartu">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/1078x800/2.png" data-lightbox="gallery-5">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/539x400/2.png" alt="Edicio Zañartu">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/1078x800/3.png" data-lightbox="gallery-5">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/539x400/3.png" alt="Edicio Zañartu">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/1078x800/4.png" data-lightbox="gallery-5">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/539x400/4.png" alt="Edicio Zañartu">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/1078x800/5.png" data-lightbox="gallery-5">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/539x400/5.png" alt="Edicio Zañartu">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/1078x800/6.png" data-lightbox="gallery-5">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/539x400/6.png" alt="Edicio Zañartu">
                                            </a>
                                        </div>
                                        <div class="item">
                                            <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/1078x800/7.png" data-lightbox="gallery-5">
                                                <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/zanartu/539x400/7.png" alt="Edicio Zañartu">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- descripcion departamento -->
                                <div class="col-md-6 order-lg-last">
                                    <div class="card-body">
                                        <h2 class="card-title">Edificio Zañartu</h2>
                                        <address class="address-edificio"><i class="fas fa-map-marker-alt"></i>
                                            Zañartu 1315, Ñuñoa</address>
                                        <div class="tabla rounded">
                                            <table class="table table-bordered table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th scope="col"><i class="fas fa-bed"></i> Dormitorios</th>
                                                        <th scope="col"><i class="fas fa-bath"></i> Baños</th>
                                                        <th scope="col"><i class="fas fa-dollar-sign"></i> Precio
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>desde <b>$337.000</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>1</td>
                                                        <td>desde <b>$410.000</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>2</td>
                                                        <td>desde <b>$500.000</b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="row g-1">
                                            <div class="col-12">
                                                <h5>Conectividad</h5>
                                                <p>Cercanía a locomoción colectiva y a estaciones de metro
                                                    <b>Rodrigo
                                                        de Araya</b> y <b>Estadio Nacional</b>
                                                </p>
                                            </div>
                                            <div class="btn-group mb-3" role="group">
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Piscina"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-piscina.png" alt="Piscina"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Quincho"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-quincho.png" alt="Quincho"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Salon Gourmet"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-salon.png" alt="Salon Gourmet"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Bicicleteros"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-bicicletero.png" alt="Bicicleteros"></button>
                                                <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Metro"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-metro.png" alt="Metro"></button>
                                            </div>
                                        </div>
                                        <div class="btn-group w-100" role="group">
                                            <a href="zanartu" class="btn btn-lg btn-secondary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3">
                                                Conocer más</a>
                                            <a href="https://my.matterport.com/show/?m=toXfw7bCYwU" target="_blank" rel="noopener nofollow" class="btn btn-lg btn-primary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3">Tour Virtual 360°</a>
                                        </div>
                                        <p class="card-text"><small class="text-muted">*Revisa los requisitos para
                                                rentar un departamento</small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- section contacto -->
    <section id="contacto">
        <div class="container-fluid bg-light">

            <div class="container">
                <div class="row py-5">
                    <div class="col-lg-6 col-12">
                        <div class="card pb-3 bg-triana">
                            <div class="card-body px-5 text-white">
                                <h5 class="text-center mb-4"><u>Requisitos para arrendar en Sazie y Zañartu</u></h3>
                                    <h6>Promedio de Renta (se puede complementar entre los habitantes)
                                </h5>
                                <ul>
                                    <li>Acreditar un ingreso mensual 3 veces el valor del canon de arrendamiento.</li>
                                </ul>
                                <h6>Antecedentes comerciales</h5>
                                    <ul>
                                        <li>Cédula de identidad o pasaporte</li>
                                        <li>Contrato de trabajo indefinido</li>
                                        <li>3 últimas liquidaciones de sueldo</li>
                                        <li>3 últimas cotizaciones AFP</li>
                                        <li>Carpeta tributaria (independientes)</li>
                                        <li>No tener antecedentes comerciales (DICOM)</li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12">
                        <h3 class="text-center mb-3">Formulario Contacto</h3>
                        <script rel="preload" type="text/javascript" src="https://clientify.net/web-marketing/superforms/script/65504.js"></script>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cta-contacto">
        <div class="cta text-light">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-sm-9">
                        <h2>Ahorra tiempo y realiza un pre-evaluación de arriendo aquí</h2>
                        <p class="pb-0">¡Arrienda hoy mismo y no pierdas la oportunidad con <b>Triana Propiedades!</b></p>
                    </div>
                    <div class="col-sm-3 text-left text-sm-right">
                        <a href="preevaluacion" class="btn btn-lg btn-outline-light">Ir a la pre-evaluación</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endwhile; ?>
<?php get_footer(); ?>