<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
    <style>
        header {
            display: none;
        }

        body {
            background-color: white !important;
        }

        .text__primary__triana {
            color: #ff2782
        }

        .bg__primary__triana {
            background-color: #00799c
        }

        .text__secondary__triana {
            color: #00799c
        }

        .btn-custom {
            background-color: #ff2782;
            border-color: #ff2782;
        }

        .card-requisitos {
            border: 2px dashed #fff;
        }
    </style>
    <section class="container-fluid pb-5" style="background: url(<?php echo get_stylesheet_directory_uri() ?>/assets/img/banner/bg-preevaluador.jpg) center center; background-repeat: no-repeat;background-size: cover;">
        <nav class="navbar navbar-expand-lg navbar-light pt-4">
            <div class="container">
                <a class="navbar-brand mx-auto" href="./">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/logotriana70x118.png" alt="Logo Triana Propiedades" class="logo-brand">
                </a>


            </div>
        </nav>
        <div class="container">
            <div class="row py-5">
                <div class="col-12">
                    <h1 class="text-center mb-3 text-white">Preevaluación de arriendo en Triana Propiedades</h1>
                    <p class="text-center text-white">Completa este formulario con tus datos y adjunta también los documentos solicitados. <br>Uno de nuestros ejecutivos te contactará a la brevedad.</p>
                </div>
            </div>
            <div class="row g-5 justify-content-center align-items-center">
                <div class="col-12 col-md-6 mx-auto col-lg-6 mb-5">
                    <h4 class="text-center text-white">Requisitos y documentación que debes adjuntar</h4>
                    <div class="card pb-3 bg-triana">
                        <div class="card-body px-5 text-white">
                                <h6>Promedio de Renta (se puede complementar entre los habitantes)</h6>
                            <ul>
                                <li>Acreditar un ingreso mensual 3 veces el valor del canon de arrendamiento.</li>
                            </ul>
                            <h6>Antecedentes comerciales</h5>
                                <ul>
                                    <li>Cédula de identidad o pasaporte</li>
                                    <li>Contrato de trabajo indefinido</li>
                                    <li>3 últimas liquidaciones de sueldo</li>
                                    <li>3 últimas cotizaciones AFP</li>
                                    <li>Carpeta tributaria (independientes)</li>
                                    <li>No tener antecedentes comerciales (DICOM)</li>
                                </ul>
                        </div>
                    </div>
                    <!-- <p class="text-white text-center"><small>*Tener renta mayor a $750.000, se puede complementar renta.</small></p> -->
                </div>
                <div class="col-12 col-md-6 mx-auto col-lg-6 mb-5">
                    <?php echo do_shortcode('[contact-form-7 id="25" title="Contact form 1"]'); ?>
                </div>
            </div>

        </div>
    </section>
<?php endwhile; ?>
<?php get_footer(); ?>