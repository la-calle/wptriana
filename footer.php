<!-- footer -->
<div class="container">
    <footer class="py-3 my-4">
        <ul class="nav justify-content-center pb-3">
            <li class="nav-item"><a href="#rivne" class="nav-link px-2 text-muted">Edificio Rivne y Elena</a></li>
            <li class="nav-item"><a href="#odesa" class="nav-link px-2 text-muted">Edificio Odesa</a></li>
            <li class="nav-item"><a href="#toesca" class="nav-link px-2 text-muted">Edificio Toesca</a></li>
            <li class="nav-item"><a href="#sazie" class="nav-link px-2 text-muted">Edificio Sazie</a></li>
            <li class="nav-item"><a href="#zanartu" class="nav-link px-2 text-muted">Edificio Zañartu</a></li>
        </ul>

        <ul class="nav justify-content-center border-bottom pb-3 mb-3">
            <li class="nav-item facebook-icon m-1">
                <a href="https://www.facebook.com/trianapropiedadescl" class="nav-link" target="_blank">
                    <i class="fab fa-facebook-f"></i>
                </a>
            </li>
            <li class="nav-item instagram-icon m-1">
                <a href="https://www.instagram.com/triana_propiedades.cl/" class="nav-link" target="_blank">
                    <i class="fab fa-instagram"></i>
                </a>
            </li>
        </ul>

        <p class="text-center text-muted">© 2021 Triana Propiedades</p>
    </footer>
</div>
<div class="widget-container">
    <style>
        .float {
            position: fixed;
            width: 60px;
            height: 60px;
            bottom: 40px;
            right: 40px;
            background-color: #25d366;
            color: #FFF;
            border-radius: 50px;
            text-align: center;
            font-size: 30px;
            box-shadow: 2px 2px 3px #999;
            z-index: 9999999;
        }

        .my-float {
            margin-top: 16px;
        }
    </style>
    <a href="https://api.whatsapp.com/send?phone=+56950746513&amp;text=Hola,%20me%20interesaria%20tener%20mas%20informaci%C3%B3n%20para%20arrendar." class="float text-white" target="_blank"><i class="fab fa-whatsapp my-float"></i>
    </a>
</div>
</main>
<?php wp_footer(); ?>
<!-- Fancybox tambien lo siento -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>
</body>

</html>