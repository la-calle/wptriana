<?php get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
    <style>
        header {
            display: none;
        }

        body {
            background-color: white !important;
        }

        .box__galeria {
            background-color: white;
            border-radius: 15px;
        }

        .text__primary__triana {
            color: #ff2782
        }

        .text__secondary__triana {
            color: #00799c
        }

        .btn-custom {
            background-color: #ff2782;
            border-color: #ff2782;
        }
    </style>

    <section>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="<?php echo site_url()?>#sazie">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/logotriana70x118.png" alt="Logo Triana Propiedades" class="logo-brand">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="btn btn-primary btn-lg" href="#contacto">
                                <i class="fas fa-paper-plane" aria-hidden="true"></i> Solicitar Información
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-triana" aria-current="page" href="https://api.whatsapp.com/send?phone=+56950746513&amp;text=Hola,%20me%20interesaria%20tener%20mas%20informaci%C3%B3n%20para%20arrendar.">
                                <i class="fab fa-whatsapp"></i> +569 5074 6513
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </section>

    <section class="container-fluid py-5" style="background-image: url(<?php echo get_stylesheet_directory_uri() ?>/assets/img/banner/test.jpg);">
        <div class="jumbotron jumbotron-fluid py-5">
            <div class="container">
                <h1 class="display-4 text-white fw-bold">Edificio Sazie</h1>
                <p class="lead text-white">Locomoción a la puerta, ubicado en pleno barrio universitario. Cercanía a <strong>Metro República</strong>.</p>
                <address class="address-edificio" style="width: auto; max-width: 300px;"><i class="fas fa-map-marker-alt" aria-hidden="true"></i> Sazié 2357, Santiago</address>
            </div>
        </div>
    </section>

    <section id="info">
        <div class="container-fluid bg-triana-light">
            <div class="container py-3">
                <div class="row my-5">
                    <div class="col-12">
                        <div class="row justify-content-center align-items-center">
                            <!-- fotos departamento -->
                            <div class="col-md-6 px-4 py-5 px-3 order-lg-first">
                                <div class="owl-carousel owl-theme galeria-sazie">
                                    <div class="item">
                                        <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/sazie/sazie-1078x800.png" data-lightbox="gallery-4">
                                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/fachadas/sazie/sazie-539x400.png" alt="Edificio Sazie">
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/1.png" data-lightbox="gallery-4">
                                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/1.png" alt="Edificio Sazie">
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/2.png" data-lightbox="gallery-4">
                                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/2.png" alt="Edificio Sazie">
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/3.png" data-lightbox="gallery-4">
                                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/3.png" alt="Edificio Sazie">
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/4.png" data-lightbox="gallery-4">
                                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/4.png" alt="Edificio Sazie">
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/5.png" data-lightbox="gallery-4">
                                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/5.png" alt="Edificio Sazie">
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/6.png" data-lightbox="gallery-4">
                                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/6.png" alt="Edificio Sazie">
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a href="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/1078x800/7.png" data-lightbox="gallery-4">
                                            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/sazie/539x400/7.png" alt="Edificio Sazie">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 order-lg-last">
                                <div class="row justify-content-center align-items-center">
                                    <div class="col-12 col-md-6">
                                        <div class="card shadow-sm">
                                            <div class="card-body text-center">
                                                <h5 class="card-title">Ambiente</h5>
                                                <h6 class="mb-2 text__secondary__triana" style="font-size: 80px;">1</h6>
                                                <p class="card-text">¡Arrienda hoy, no esperes más!</p>
                                                <a href="https://my.matterport.com/show/?m=wU8LgMYpPbT" target="_blank" rel="noopener nofollow" class="btn btn-lg btn-primary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3 w-100">Tour Virtual 360°</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6 mt-4 mt-lg-0">
                                        <div class="card shadow-sm">
                                            <div class="card-body text-center">
                                                <h5 class="card-title">Desde</h5>
                                                <h6 class="mb-2 text__primary__triana" style="font-size: 30px;">$290.000</h6>
                                                <p class="card-text">Valor del arriendo mensual</p>
                                                <a href="#contacto" class="btn btn-lg btn-secondary btn-triana border-0 rounded-0 hvr-underline-from-right mb-3 w-100">
                                                    Quiero más información</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row my-5">
                                    <div class="col-12 mx-auto">
                                        <h5>Características Generales</h5>
                                        <p>Conoce los servicios y características generales que podrás encontrar en Edificio Sazie, arrienda hoy mismo!</p>
                                        <div class="btn-group border" role="group">
                                            <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Quincho"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-quincho.png" alt="Quincho"></button>
                                            <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Lavanderia"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-lavanderia.png" alt="Lavanderia"></button>
                                            <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Bicicleteros"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-bicicletero.png" alt="Bicicleteros"></button>
                                            <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Cowork"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-coworking.png" alt="Cowork"></button>

                                        </div>
                                        <div class="btn-group border" role="group">
                                            <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Acceso control biométrico"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-biometrico.png" alt="Biometrico"></button>
                                            <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Seguridad 24x7"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-seguridad.png" alt="Seguridad 24x7"></button>
                                            <button type="button" class="btn bg-light hvr-float" data-bs-toggle="tooltip" data-bs-placement="top" title="Metro"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/iconos/icon-triana-metro.png" alt="Metro"></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    </section>

    <section id="clientify">
        <div class="container py-3">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-center">Formulario de contacto</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 offset-md-3">
                    <script rel="preload" type="text/javascript" src="https://clientify.net/web-marketing/superforms/script/65506.js"></script>
                </div>
            </div>
        </div>
    </section>

    <section id="contacto">
        <div class="cta text-light">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-sm-9">
                        <h2>Ahorra tiempo y realiza un pre-evaluación de arriendo aquí</h2>
                        <p class="pb-0">¡Arrienda hoy mismo y no pierdas la oportunidad con <b>Triana Propiedades!</b></p>
                    </div>
                    <div class="col-sm-3 text-left text-sm-right">
                        <a href="preevaluacion" target="_blank" class="btn btn-lg btn-outline-light">Ir a la pre-evaluación</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>
<?php get_footer(); ?>