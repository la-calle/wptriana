<!doctype html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Triana Propiedades - Departamentos en renta en Santiago de Chile.</title>
    <link rel="shortcut icon" href="https://trianapropiedades.cl/wp-content/uploads/2021/06/1-logo%20triana%20sin%20letras.png" type="image/x-icon">
    <script src="https://kit.fontawesome.com/c9d71cc307.js" crossorigin="anonymous"></script>
    <!-- Analytics y Pixel -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-207007432-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-207007432-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '3030108657202949');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=3030108657202949&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->
    <?php wp_head(); ?>
</head>

<body class="body-triana">
    <!-- menu Triana -->
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="./">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/logotriana70x118.png" alt="Logo Triana Propiedades" class="logo-brand">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                        <div class="me-2">
                            <div class="dropdown">
                                <button class="btn btn-lg btn-secondary-triana dropdown-toggle" type="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                                    <i class="fas fa-map-marker-alt"></i> Encuentra tu arriendo
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <li><a class="dropdown-item" href="#rivne">Edificio Rivne y Elena</a></li>
                                    <li><a class="dropdown-item" href="#odesa">Edificio Odesa</a></li>
                                    <li><a class="dropdown-item" href="#toesca">Edificio Toesca</a></li>
                                    <li><a class="dropdown-item" href="#sazie">Edificio Sazie</a></li>
                                    <li><a class="dropdown-item" href="#zanartu">Edificio Zañartu</a></li>
                                </ul>
                            </div>
                        </div>
                        </li>
                        
                        <li class="nav-item">
                            <a class="btn btn-primary btn-lg" href="#contacto">
                                <i class="fas fa-paper-plane"></i> Solicitar Información
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-triana" aria-current="page" href="https://api.whatsapp.com/send?phone=+56950746513&amp;text=Hola,%20me%20interesaria%20tener%20mas%20informaci%C3%B3n%20para%20arrendar.">
                                <i class="fab fa-whatsapp"></i> +569 5074 6513
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <main>