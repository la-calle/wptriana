(function ($) {
    $('.galeria-rivne').owlCarousel({
        loop: true,
        margin: 10,
        startPosition: 0,
		singleItem: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 1
            }
        }
    })
	
	$('.galeria-odesa').owlCarousel({
        loop: true,
        margin: 10,
        startPosition: 0,
		singleItem: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 1
            }
        }
    })
	
	$('.galeria-toesca').owlCarousel({
        loop: true,
        margin: 10,
        startPosition: 0,
		singleItem: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 1
            }
        }
    })
	
	$('.galeria-sazie').owlCarousel({
        loop: true,
        margin: 10,
        startPosition: 0,
		singleItem: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 1
            }
        }
    })
	
	$('.galeria-zanartu').owlCarousel({
        loop: true,
        margin: 10,
        startPosition: 0,
		singleItem: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 1
            }
        }
    })

    $(function() {
        // asumiendo que la página cuenta con un input[type='text'] con 
        // class "rut", la siguiente línea activa el plugin en dicho input.
        $("input.rut").rut();
    })

    
	/*********************************************
	** FLOATING LABELS FOR CF7
	*********************************************/
	$(".wpcf7 .form-control").focus(function(){
		$(this).parent().parent().addClass('active');
	}).blur(function(){
		var cval = $(this).val()
		if(cval.length < 1) {$(this).parent().parent().removeClass('active');}
	})
})(jQuery);

var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
})

/**/
console.log('ready uebeats')